import 'package:flutter/material.dart';
import 'tab_home.dart';
import 'tab_mail.dart';
import 'tab_profile.dart';


class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    MailPage(),
    ProfilePage()
  ];

  final List<MaterialColor> _color = [
    Colors.green,
    Colors.blue,
    Colors.cyan
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Title Bar'),
        backgroundColor: _color[_currentIndex],
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text('Home')),

          BottomNavigationBarItem(
              icon: new Icon(Icons.mail),
              title: new Text('Mail')),

          BottomNavigationBarItem(
              icon: new Icon(Icons.person),
              title: new Text('Profile'))
        ],
      ),
    );
  }

  void onTabTapped(int index){
    setState(() {
      _currentIndex = index;
    });
  }
}
